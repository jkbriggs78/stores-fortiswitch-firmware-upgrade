import json
import paramiko
from paramiko_expect import SSHClientInteraction
import threading
import time
from getpass import getpass
import socket
socket.setdefaulttimeout(5)
import os
import sys
import datetime as dt
from colorama import init,Fore,Style
import logging
from tqdm import tqdm

# Thread configuration
maxthreads = 11
smphr = threading.Semaphore(value=maxthreads)
threads = []
# logging.basicConfig()
paramiko.util.log_to_file("./ssh_log.txt", level = "INFO")

# SSH function to connect and run commands
def ssh(device,password,username,ftp_ip,ftp_user,ftp_passwd,filename):
		print(time.time())
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		switch_prompt = '.* # .*'
		
		try:
			print('Connecting to IP ', device)
			client.connect(device,username=username,password=password,timeout=1200)
			stdin, stdout, stderr = client.exec_command("")
			output = stdout.read().decode('ascii').strip("\n")
			commands = ['execute set-next-reboot primary','execute stage image ftp ','y','execute reboot']

			with SSHClientInteraction(client,timeout=600, display=False) as interact:
				print('Login success..')					
				interact.send(commands[0])
				interact.expect(switch_prompt)
				print("Copying " , filename , "file from", ftp_ip)											  
				interact.send(commands[1] + filename + " " + ftp_ip + " " + ftp_user + " " + ftp_passwd)
				interact.expect('.*(y/n)*')
				interact.send(commands[2])
				interact.expect(['.*... pass.*','.*Return code -1.*','.*Return code -28.*'])
				
				if interact.last_match == '.*... pass.*':
					print('Firmware transfer successful, Firmware upgrade in progress...please wait')
					#time.sleep(30)
					interact.expect(switch_prompt)
					interact.send('diag sys flash list')
					interact.expect(switch_prompt)
					print('Succeeded in verifying image written to flash, rebooting...')
					interact.send(commands[3])
					interact.expect('.*(y/n)*')
					interact.send(commands[2])
					interact.expect('.*System is rebooting...')
					time.sleep(30)
					with open("./device_sucess.txt", "a") as f:
						print(device, file=f)	
					print(output)

				elif interact.last_match == '.*Return code -1.*':
					with open("./corrupt_failed_ftp_devices.txt", "a") as f:
						print(device + " has corrupt image", file=f)	
					interact.send('exit')
					interact.expect(switch_prompt)
					print(device + " has corrupt image")
					print(output)

				elif interact.last_match == '.*Return code -28.*':
					with open("./corrupt_failed_ftp_devices.txt", "a") as f:
						print(device + " failed FTP", file=f)	
					print("File not found on FTP")
					interact.send('exit')

		except:
			print(time.time())
			print(device + " something went wrong!")
			with open("./failed_devices.txt", "a") as f:
				print(device, file=f)	

		finally:
			client.close()

def calcProcessTime(starttime, cur_iter, max_iter):

	telapsed = time.time() - starttime
	testimated = (telapsed/cur_iter)*(max_iter)
	finishtime = starttime + testimated
	finishtime = dt.datetime.fromtimestamp(finishtime).strftime("%H:%M:%S")  # in time
	lefttime = testimated-telapsed  # in seconds
	return (int(telapsed), int(lefttime), finishtime)

def main():
    username = input("Switch Username: ")
    #username = 'gsadmin'
    password = getpass()
    #password = '********'
    ftp_ip= input("FTP IP Address: ")
    #ftp_ip= '********'
    ftp_user= input("FTP username: ")
    #ftp_user= 'ftpuser1'
    ftp_passwd = getpass()
    #ftp_passwd = '********'
    filename = input("Firmware filename: ")
    #filename = '************'
    # Read ip addresses from file and add them to array
    with open("./fsw.txt") as file:
        ipaddresses = []
        ipaddresses = file.read().splitlines()
        print(f"There are",len(ipaddresses),"ip addresses in the list")
    
    start = time.time()
    cur_iter = 0
    max_iter = len(ipaddresses)
    # Connect to ip addresses from array 'ipadresses' in multiple threads

    for device in ipaddresses:
        smphr.acquire()
        t = threading.Thread(target=ssh,args=(device,password,username,ftp_ip,ftp_user,ftp_passwd,filename))
        t.start()
        threads.append(t)
        
    for t in threads:
        cur_iter += 1
        prstime = calcProcessTime(start,cur_iter,max_iter)
        print(f"Time elapsed: %s(s), time left: %s(s), estimated finish time: %s"%prstime)
        t.join()
        smphr.release()

if __name__ == "__main__":
    main()

