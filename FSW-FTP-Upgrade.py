import json
import paramiko
from paramiko_expect import SSHClientInteraction
import time
from getpass import getpass

# SSH function to connect and run commands
def ssh(device,password,username,ftp_ip,ftp_user,ftp_passwd,filename):
		print(time.time())
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		switch_prompt = '.* # .*'
		
		try:
			print('connecting to ip ', device)
			client.connect(device,username=username,password=password,timeout=1200)
			stdin, stdout, stderr = client.exec_command("")
			output = stdout.read().decode('ascii').strip("\n")
			commands = ['execute set-next-reboot primary','execute stage image ftp ','y','execute reboot']

			with SSHClientInteraction(client,timeout=600, display=True) as interact:
				interact.send(commands[0])
				interact.expect(switch_prompt)
				interact.send(commands[1] + filename + " " + ftp_ip + " " + ftp_user + " " + ftp_passwd)
				interact.expect('.*(y/n)*')
				interact.send(commands[2])
				interact.expect(['.*... pass.*','.*Return code -1.*','.*Return code -28.*'])
				
				if interact.last_match == '.*... pass.*':
					time.sleep(30)
					interact.expect(switch_prompt)
					interact.send('diag sys flash list')
					interact.expect(switch_prompt)
					interact.send(commands[3])
					interact.expect('.*(y/n)*')
					interact.send(commands[2])
					interact.expect('.*System is rebooting...')
					time.sleep(30)
					with open("./device_sucess.txt", "a") as f:
						print(device, file=f)	
					print(output)

				elif interact.last_match == '.*Return code -1.*':
					with open("./corrupt_failed_ftp_devices.txt", "a") as f:
						print(device + " has corrupt image", file=f)	
					interact.send('exit')
					interact.expect(switch_prompt)
					print(device + " has corrupt image")
					print(output)

				elif interact.last_match == '.*Return code -28.*':
					with open("./corrupt_failed_ftp_devices.txt", "a") as f:
						print(device + " failed FTP", file=f)	
					print("File not found on FTP")
					interact.send('exit')

		except:
			#print(time.time())
			print(device + " something when wrong!")
			with open("./failed_devices.txt", "a") as f:
				print(device, file=f)	

		finally:
			client.close()

def main():


		username = input("Switch Username: ")

		password = getpass()

		ftp_ip= input("FTP IP Address: ")

		ftp_user= input("FTP username: ")

		ftp_passwd = getpass()

		filename = input("Firmware filename: ")

		# reads file fgt.txt in the local directory line by line IPs of switches to ssh and upgrade

		with open("./fgt.txt") as file:

			for device in file:
				device = device.rstrip()
				ssh(device,password,username,ftp_ip,ftp_user,ftp_passwd, filename)

if __name__ == "__main__":
    main()

